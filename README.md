A CSS filter generator for converting from black to a target color. See deployment at https://mrpie5.gitlab.io/css-filter-generator

# Credits
- Base code was written by Barrett Sonntag, copied from https://codepen.io/sosuke/pen/Pjoqqp
- Pages deployment taken from https://gitlab.com/pages/plain-html
